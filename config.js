exports.base_config = {

    // The title is shown on every page
    TITLE:                  "Default Page Title",
    // Information for the page's meta tag
    DESCRIPTION:            "Default Description",
    KEYWORDS:               "keywords1, keywords2",
    // This picture will be displayed on the left side of the page
    AVATAR_URL:             "/header.jpg",
    // This name will be displayed on the left side of the page and top of the about page
    DISPLAY_NAME:           "Username",
    // You can write something about yourself using raw html, this will be displayed on the about page
    INTRODUCTION:           "Write something about yourself",
    // Features enabled, a list will be displayed on the left side of the page
    ENABLED_FEATURES:       ['AboutMe', 'Blog', 'Github', 'Email']

};

exports.features_config = {

    "AboutMe":              {
        // If you don't know the meaning of this, DO NOT MODIFY THIS!
        source:             "/about",
        // You can change this url to your own site, this will be displayed when mouse on the "AboutMe" button
        url:                "http://www.example.com",
    },

    "Blog":                 {
        // If you don't know the meaning of this, DO NOT MODIFY THIS!
        source:             "/blog",
        // You can change this url to your own Wrodpress site url, this will be displayed when mouse on the "Blog" button
        url:                "http://blog.example.com",
        // Change the following url to your Wrodpress RSS address
        rss_url:            "http://blog.example.com/feed/"
    },

    "Github":               {
        // If you don't know the meaning of this, DO NOT MODIFY THIS!
        source:             "/github",
        // Change the following username to your own Github username
        username:           "Username",
        // You can change this url to your own github page url, this will be displayed when mouse on the "Github" button
        url:                "https://github.com/Username",
        // If you don't know the meaning of this, DO NOT MODIFY THIS!
        api_url:            "https://api.github.com/users/"
    },

    "Email":                {
        // If you don't know the meaning of this, DO NOT MODIFY THIS!
        source:             "",
        // Change the email to your own email(do not remove the 'mailto' prefix)
        url:                "mailto:Username@example.com",
        // If you don't know the meaning of this, DO NOT MODIFY THIS!
        direct_url:         true
    }

};
