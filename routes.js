var index = require('./controllers/index');
var features = require('./controllers/features');
var blog = require('./controllers/blog');
var github = require('./controllers/github');
var about = require('./controllers/about');

module.exports = function (app) {
    app.get('/', index.index);
    app.get('/features', features.index);
    app.get('/blog', blog.index);
    app.get('/github', github.index);
    app.get('/about', about.index);
};
