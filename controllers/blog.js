var util = require('util');
var parser = require('rssparser');
var base = require('./base');
var config = require('../config').features_config;

var blog = function () {
    base.call(this);

    var self = this;

    var _index = function (req, res) {
        if ( config['Blog'] && config['Blog'].rss_url) {
            parser.parseURL(config['Blog'].rss_url, function (err, out) {
                if ( !out ) {
                    out = {};
                    out.items = [];
                }
                self.render_body.extend({
                    logs:       out.items,
                    blog_url:   config['Blog'].url
                });
                return res.render('blog', self.render_body);
            });
        }
        else {
            return res.render('blog', self.render_body);
        }
    };

    return {
        index:  _index
    };
};

module.exports = blog();

util.inherits(blog, base);
