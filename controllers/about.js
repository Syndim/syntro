var util = require('util');
var base = require('./base');
var config = require('../config').features_config;

var about = function () {
    base.call(this);

    var self = this;

    var _index = function (req, res) {
        res.render('about', self.render_body);
    };

    return {
        index:  _index
    };

};

module.exports = about();

util.inherits(about, base);
