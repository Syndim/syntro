var base_config = require('../config').base_config;

module.exports = function () {

    var self = this;

    self.render_body = base_config;

    self.render_body.extend = function (options) {
        for ( var key in options ) {
            self.render_body[key] = options[key];
        }
    };

};
