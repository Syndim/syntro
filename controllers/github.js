var util = require('util');
var base = require('./base');
var config = require('../config').features_config;
var https = require('https');

var github = function () {
    base.call(this);

    var self = this;

    var _index = function (req, res) {
        if ( config['Github'] ) {
            var user_url = config['Github'].api_url + config['Github'].username;
            var repo_url = user_url + '/repos'
            https.get(repo_url, function (repo_res) {
                var content = '';
                repo_res.on('data', function (chunk) {
                    content += chunk;
                });
                repo_res.on('end', function () {
                    //console.log(content);
                    if ( content.indexOf('<') == 0 ) {
                        self.render_body.extend({
                            repos:  []
                        });
                    }
                    else {
                        var result = JSON.parse(content);
                        self.render_body.extend({
                            repos:  result
                        });    
                    }
                    return res.render('github', self.render_body);
                });
            });
        }
        else {
            return res.render('github', self.render_body);
        }
    };

    return {
        index:  _index
    };

};

module.exports = github();

util.inherits(github, base);
