var util = require('util');
var base = require('./base');

var index = function () {
    base.call(this);

    var self = this;

    var _index = function (req, res) {
        res.render('index', self.render_body);
    };

    return {
        index:  _index
    };
};

util.inherits(index, base);
module.exports = index();
